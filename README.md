# Test task - todo/done - drag between columns

## Getting Started

First, clone the repository:
`git clone git@gitlab.com:nataliia_l/test-matterway-project.git`

Go to `test-matterway-project`

In the project directory run:

### `npm i`
### `npm start`

Project will be open in browser `http://localhost:3000/`
