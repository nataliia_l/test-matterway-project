import {DraggableLocation} from "react-beautiful-dnd";

export const move = (
    source: any,
    destination: any,
    droppableSource: DraggableLocation,
    droppableDestination: DraggableLocation
) => {
    const sourceClone = source.slice();
    const destClone = destination.slice();
    const [removed] = sourceClone.splice(droppableSource.index, 1);

    destClone.splice(droppableDestination.index, 0, removed);

    return {
        [droppableSource.droppableId]: sourceClone,
        [droppableDestination.droppableId]: destClone
    };
};

export const reorder = (list: any, startIndex: number, endIndex: number) => {
    const result = list.slice();
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};
