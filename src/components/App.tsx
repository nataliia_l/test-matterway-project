import React, {useEffect, useState} from 'react';
import * as data from '../tasks.json';
import {DragDropContext, Droppable, DroppableProvided, DropResult} from 'react-beautiful-dnd';
import {move, reorder} from "../helper/drag";
import {DONE, TODO} from "../helper/constant";
import {Task} from "../interfaces/Task";
import DraggableBlock from "./DraggableBlock";

function App() {
    const tasksArray: any = data.tasks;

    const [todoItems, setTodoItems] = useState<Task[]>([]);
    const [doneItems, setDoneItems] = useState<Task[]>([]);

    useEffect(() => {
        let todo: Task[] = [], done: Task[] = [];
        tasksArray.map((task: Task) => {
            todo = task.status === 'todo' ? [...todo, task] : todo;
            done = task.status === 'done' ? [...done, task] : done;
        })

        setTodoItems(todo);
        setDoneItems(done);
    }, []);

    const getList = (idList: string) => {
        return idList === TODO ? todoItems : doneItems;
    };

    const onDragEnd = (result: DropResult) => {
        const {source, destination} = result;
        if (!destination) {
            return;
        }
        if (source.droppableId === destination.droppableId) {
            const items = reorder(getList(source.droppableId), source.index, destination.index);
            source.droppableId === TODO ? setTodoItems(items) : setDoneItems(items);
        } else {
            const resultArray = move(getList(source.droppableId), getList(destination.droppableId), source, destination);
            setTodoItems(resultArray.todo);
            setDoneItems(resultArray.done);
        }
    };

    return (
        <div className="container">
            <DragDropContext onDragEnd={onDragEnd}>
                <div>
                    <h2>TO-DO</h2>
                    <Droppable droppableId={TODO}>
                        {(provided: DroppableProvided) => (
                            <div ref={provided.innerRef} className="block"><DraggableBlock type={TODO} items={todoItems}/></div>
                        )}
                    </Droppable>
                </div>
                <div>
                    <h2>DONE</h2>
                    <Droppable droppableId={DONE}>
                        {(provided: DroppableProvided) => (
                            <div className="block" ref={provided.innerRef}><DraggableBlock type={DONE} items={doneItems}/></div>
                        )}
                    </Droppable>
                </div>
            </DragDropContext>
        </div>
    )
}

export default App;
