import React from 'react';
import {Draggable, DraggableProvided,} from 'react-beautiful-dnd';
import {Task} from "../interfaces/Task";
import {DONE} from "../helper/constant";

const DraggableBlock = (props: { type: string, items: Task[] }) => {
    return <>
        {(props.items.map((item: Task, index: number) => {
            return <Draggable key={item.id} draggableId={item.id} index={index}>
                {(provided: DraggableProvided) => (
                    <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        className="item"
                        style={{...provided.draggableProps.style}}
                    >
                        <p style={props.type === DONE ? {textDecoration: "line-through"} : {}}>{item.title}</p>
                    </div>
                )}
            </Draggable>
        }))}
    </>
}

export default DraggableBlock;
